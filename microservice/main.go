package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type helloWorldResponse struct {
	Message string `json:"message"`              // output with lower case
	Author  string `json:"-"`                    // don't output
	Date    string `json:",omitempty"`           // don't output if empty
	Id      int    `json:",omitempty,id,string"` // rename and convert to string
}

type helloWorldRequest struct {
	Name string `json:"name"`
}

func main() {
	port := 8080
	http.HandleFunc("/helloworld", helloWorldHandler)
	log.Printf("Server starting on port %v\n", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", port), nil)) // Handler = nil => DefaultServeMux
}

func helloWorldHandler(w http.ResponseWriter, r *http.Request) {
	var request helloWorldRequest
	decoder := json.NewDecoder(r.Body) // json.NewDecoder more performant than using ioutil.ReadAll and then json.Unmarshal

	err := decoder.Decode(&request)
	if err != nil {
		http.Error(w, "Bad request!", http.StatusBadRequest)
		return
	}

	response := helloWorldResponse{Message: "Hello, " + request.Name}

	encoder := json.NewEncoder(w) // json.NewEncoder more performant than using json.Marshall and then Fprint
	encoder.Encode(&response)
}
